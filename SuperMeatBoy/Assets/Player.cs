﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class Player : MonoBehaviour {

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private float maxS = 11f;
    private bool jump = false;
    private bool grounded = true;
    private bool wallJump = false;
    private int wallCout = 90;
    private bool die = false;
    private Vector2 positionPlayer;
    private Animator anim;
    private bool flipped = false;
    

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // check the colission betwin player and eviorment
    private void OnCollisionEnter2D(Collision2D coll)
    {

            if ((coll.gameObject.tag == "Floor") || (coll.gameObject.tag == "Platform"))
        {
            anim.SetBool("jump", false);
            grounded = true;
            wallJump = false;
            Debug.Log("floor");
        }
        if (coll.gameObject.tag == "Wall")
        {
            wallJump = true;
            Debug.Log("wall");
        }
        if (coll.gameObject.tag == "Pinchos" || coll.gameObject.tag == "Pendulo" || coll.gameObject.tag == "Roca")
        {
            anim.SetBool("die", true);
            die = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Checkpoint")
        {
            Debug.Log("checkpoint");
            positionPlayer = rb2d.position;
        }
        if(coll.gameObject.tag == "Sierra")
        {
            anim.SetBool("die", true);
            die = true;
        }
    }


    // Update is called once per frame
    void FixedUpdate(){

        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        jump = Input.GetButtonDown("Jump");

            if (Input.GetButton("Run"))
            {
                Debug.Log("run mode");
                rb2d.velocity = new Vector2(move * maxS*2, rb2d.velocity.y);
            }
        
            if (die)
            {
                anim.SetBool("die", false);
                rb2d.position = positionPlayer;
                die = false;
            }


            if (jump && grounded)
            {
                anim.SetBool("jump", true);
                rb2d.AddForce(new Vector2(0, 8), ForceMode2D.Impulse);
                grounded = false;
            }

            if ( wallJump && !grounded )
            {
                wallCout--;
                Debug.Log("walljump on");
                rb2d.velocity = new Vector2(0, -1);
                rb2d.gravityScale = 0;

                    if (jump)
                    {
                        Debug.Log("jump when walljump");
                        rb2d.AddForce(new Vector2(move, 8), ForceMode2D.Impulse);
                        rb2d.gravityScale = 3;
                        wallJump = false;
                        wallCout = 90;                     
                    }

                    if (wallCout == 0)
                    {
                        wallCout = 90;
                        rb2d.gravityScale = 3;
                        wallJump = false;
                    }
            }

        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("run", true);
        }
        else
        {
            anim.SetBool("run", false);
        }

    }
}
