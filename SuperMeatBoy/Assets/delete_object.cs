﻿using UnityEngine;
using System.Collections;

public class delete_object : MonoBehaviour {

    private Rigidbody2D delete = null;

	// Use this for initialization
	void Start () {
        delete = GetComponent<Rigidbody2D>();
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update () {
	
	}
}
