﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    private Rigidbody2D movingPlatform = null;
    public float movingTimeX;
    public float movingTimeY;
    private float moveTimerX = 0;
    private float moveTimerY = 0;
    public int speed = 0;
    private int moveSpeedX = 1;
    private int moveSpeedY = 1;




    // Use this for initialization
    void Start () {
        movingPlatform = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        moveTimerX++;
        moveTimerY++;
        if (movingTimeX == moveTimerX)
        {
            moveSpeedX = moveSpeedX * -1;
            moveTimerX = 0;
        }
        if (movingTimeY == moveTimerY)
        {
            moveSpeedY = moveSpeedY * -1;
            moveTimerY = 0;
        }
        movingPlatform.velocity = new Vector2(speed * moveSpeedX, speed* moveSpeedY);

    }
}
